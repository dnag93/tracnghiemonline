let questionList = [];
const getDataFromDB = () => {
  axios({
    method: "GET",
    url: "../data/DeThiTracNghiemMau.json",
  })
    .then((res) => {
      mapDataToObject(res.data);
      console.log(questionList);
      createQuestionList();
    })
    .catch((err) => {
      console.log(err);
    });
};

const mapDataToObject = (data) => {
  questionList = data.map((dataItem) => {
    const { questionType, _id, content, answers } = dataItem;

    // if (questionType === 1) {
    //   const newQuestion = new MultipleChoice(
    // questionType,
    // _id,
    // content,
    // answers
    //   );
    //   return newQuestion;
    // }

    switch (dataItem.questionType) {
      case 1: {
        return new MultipleChoice(questionType, _id, content, answers);
      }
      case 2: {
        return new FillInBlank(questionType, _id, content, answers);
      }
      default:
        break;
    }
  });
  // return [];
};

// const mapDataToObject = (data) => {
//     for (let dataItem of data){
//         // destructuring
//         const {questionType, _id, content, answers} = dataItem;
//         if (questionType === 1) {
//             const newQuestion = new MultipleChoice(
//               questionType,
//               _id,
//               content,
//               answers
//             );
//             questionList.push(newQuestion);
//           } else {
//             const newQuestion = new FillInBlank(
//               questionType,
//               _id,
//               content,
//               answers
//             );
//             questionList.push(newQuestion);
//           }
//     }
// };

const createQuestionList = () => {
  let content = "";
  for (let i in questionList) {
    if (questionList[i]) {
      content += questionList[i].render(+i + 1);
    }
  }
  document.getElementById("content").innerHTML = content;
};

const handleSubmit = () => {
  let result = 0;
  for (let item of questionList) {
    if (item) {
      item.checkExact() && result++;
    }
  }
  alert(`Tổng số câu đúng: ${result}`);
};

getDataFromDB();
