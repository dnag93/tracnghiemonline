class FillInBlank extends Question {
  constructor(questionType, _id, content, answers) {
    super(questionType, _id, content, answers);
  }

  render(index) {
    return `
            <h5>Câu hỏi ${index}: ${this.content}?</h5>
            <input type="text" class="form-control w-50" />
        `;
  }
}
