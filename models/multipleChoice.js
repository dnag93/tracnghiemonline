class MultipleChoice extends Question {
  // rest - gom tham số truyền vào hàm ~ overloading hàm
  constructor(...args) {
    // spread - bung giá trị
    super(...args);
  }

  renderAnswer() {
    let content = "";
    for (let i = 0; i < this.answers.length; i++) {
      content += `
        <div>
            <input 
                value="${this.answers[i]._id}"  
                class="answer-${this._id}" 
                name="answer-${this._id}" 
                type = "radio" />
            <span>${this.answers[i].content}</span>
        </div>
        `;
    }
    return content;
  }

  render(index) {
    // trả về giao diện của câu hỏi này
    // Trong content có sẵn dấu ?
    return `
        <h4>Câu hỏi ${index}: ${this.content}</h4>
        ${this.renderAnswer()}
        `;
  }

  checkExact() {
    const inputs = document.getElementsByClassName(`answer-${this._id}`);
    let ansId;

    // for (let item of inputs) {
    //   if (item.checked === true) {
    //     ansId = item.value;
    //     // vì ta đang gán id câu hỏi cho value của các câu trả lời ???
    //     break;
    //   }
    // }

    // find Index with ES6, nếu ko thấy sẽ trả về undefined
    // let foundAns = inputs.find ( item => { return item.checked})
    // ko chạy được vì find chỉ xài với array, phải convert qua array
    let foundAns = Array.from(inputs).find((item) => {
      return item.checked;
    });

    if (foundAns) {
      ansId = foundAns.value;
      console.log("ans_id checked", ansId);
      for (let item of this.answers) {
        if (item._id === ansId) {
          console.log(item);
          return item.exact;
        }
      }
    }
    // => {input}
    return false;
    // không chọn sẽ mặc định là trả lời sai
  }
}

// phải có super trong constructor nếu ko class mulQ con sẽ overload func class cha

/* 
constructor(...args){
    => args = [1,2,"cau hoi", [1,2,3]]
    super(...args)
}
*/

// let question = new MultipleChoice(1, 2, "cau hoi 1: Nội dung", [
//   {
//     _id: "2",
//     exact: false,
//     content: "Đáp án 1 cho câu 1",
//     STT: null,
//   },
//   {
//     _id: "3",
//     exact: false,
//     content: "Đáp án 2 cho câu 1",
//     STT: null,
//   },
//   {
//     _id: "4",
//     exact: false,
//     content: "Đáp án 3 cho câu 1",
//     STT: null,
//   },
//   {
//     _id: "5",
//     exact: true,
//     content: "Đáp án 4 cho câu 1",
//     STT: null,
//   },
// ]);

// console.log("1 multipule choice", question);
// console.log("2",question.render());
